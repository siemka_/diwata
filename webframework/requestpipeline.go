package webframework

import (
	"diwata/model"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/fatih/color"
	"github.com/golang/protobuf/proto"
)

type CompositeData struct {
	R        *http.Request
	User     *model.AuthorizedUser
	JSONMap  *model.JSON
	JSONList *model.List
}

type CompositableWrapper interface {
	Composite(ctx *model.AppContext, cd CompositeData) ([]byte, int, http.Header, error)
}

type JSONOperator struct {
	Handle func(*model.AppContext, CompositeData) (interface{}, int)
}

type ProtobufOperator struct {
	Handle func(*model.AppContext, CompositeData) (interface{}, int)
}

type FileHandlerResult struct {
	FileContent []byte
	Filename    string
}

type FileOperator struct {
	Handle func(*model.AppContext, CompositeData) (interface{}, int)
}

type FinalWrapper struct {
	Ctx         *model.AppContext
	NextWrapper CompositableWrapper
}

type UserWrapper struct {
	NextWrapper CompositableWrapper
	UserLevel   int
}

type CacheWrapper struct {
	NextWrapper CompositableWrapper
}

type JSONDecoderWrapper struct {
	NextWrapper CompositableWrapper
}

func (wrapper FinalWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	cd := CompositeData{R: r}

	start := time.Now()
	bytez, code, header, err := wrapper.NextWrapper.Composite(wrapper.Ctx, cd)
	elapsed := time.Since(start)

	if err != nil {
		http.Error(w, "{\"status\":\"error\", \"error\":\""+err.Error()+"\"}", code)
		PrintfColored(color.BgMagenta, "HANDLER ERROR")
		PrintfColored(color.FgMagenta, " "+cd.R.RequestURI+" ("+elapsed.String()+")\n")
		return
	}
	if code >= 400 {
		http.Error(w, "{\"status\":\"error\", \"data\":"+string(bytez)+"}", code)
		PrintfColored(color.BgRed, "CODE ERROR "+strconv.Itoa(code))
		PrintfColored(color.FgRed, " "+cd.R.RequestURI+" ("+elapsed.String()+")\n")
		return
	}
	if code == 307 {
		PrintfColored(color.BgYellow, "REDIRECT")
		PrintfColored(color.FgYellow, " "+cd.R.RequestURI+" ("+elapsed.String()+")\n")
		http.Redirect(w, cd.R, string(bytez), http.StatusTemporaryRedirect)
		return
	}
	PrintfColored(color.BgGreen, "OK")
	PrintfColored(color.FgGreen, " "+cd.R.RequestURI+" ("+elapsed.String()+")\n")
	w.WriteHeader(code)
	for key, value := range header {
		w.Header().Set(key, value[0])
	}
	_, err = w.Write(bytez)
	if err != nil {
		Error(err)
		http.Error(w, "{\"status\":\"error\", \"error\":\""+err.Error()+"\"}", 500)
	}
}

func (operator JSONOperator) Composite(ctx *model.AppContext, cd CompositeData) ([]byte, int, http.Header, error) {
	obj, status := operator.Handle(ctx, cd)
	maybeError, ok := obj.(error)
	if ok {
		return nil, status, nil, ReturnError(maybeError)
	}
	bytez, err := json.Marshal(obj)
	if err != nil {
		return nil, status, nil, ReturnError(maybeError)
	}
	return bytez, status, http.Header{}, nil
}

func (operator ProtobufOperator) Composite(ctx *model.AppContext, cd CompositeData) ([]byte, int, http.Header, error) {
	obj, status := operator.Handle(ctx, cd)
	switch v := obj.(type) {
	case error:
		return nil, status, nil, ReturnError(v)
	case string:
		if status != 307 {
			return nil, 500, nil, ReturnError(errors.New("wrong redirect status"))
		}
		return []byte(v), 307, http.Header{}, nil
	case proto.Message:
		out, err := proto.Marshal(v)
		if err != nil {
			return nil, 500, nil, ReturnError(err)
		}
		h := http.Header{}
		h.Set("Content-Type", "application/octet-stream")
		return out, status, h, nil
	default:
		return nil, 500, nil, ReturnError(errors.New("unknown type" + reflect.TypeOf(v).String()))
	}
}

func (operator FileOperator) Composite(ctx *model.AppContext, cd CompositeData) ([]byte, int, http.Header, error) {
	obj, status := operator.Handle(ctx, cd)
	if status >= 400 {
		return nil, 500, nil, ReturnError(errors.New("wrong status " + strconv.Itoa(status)))
	}
	switch v := obj.(type) {
	case error:
		return nil, status, nil, ReturnError(v)
	case FileHandlerResult:
		hd := http.Header{}
		hd.Set("Content-Type", cd.R.Header.Get("Content-Type"))
		hd.Set("Content-Description", "File Transfer")
		hd.Set("Content-Disposition", "attachment; filename="+v.Filename)
		hd.Set("Content-Transfer-Encoding", "binary")
		hd.Set("Expires", "0")
		hd.Set("Cache-Control", "must-revalidate")
		hd.Set("Pragma", "public")
		return v.FileContent, status, hd, nil
	default:
		return nil, 500, nil, ReturnError(errors.New("unknown type" + reflect.TypeOf(v).String()))
	}
}

//PrintfColored prints str with color col
func PrintfColored(col color.Attribute, str string) {
	_, err := color.New(col).Printf(str)
	if err != nil {
		Error(err)
	}
}

func ValidateUser(token string, Tokens *map[string]*model.AuthorizedUser) (bool, *model.AuthorizedUser) {
	user, exists := (*Tokens)[token]
	if !exists {
		return false, nil
	}
	return true, user
}

func GetToken(r *http.Request) (string, error) {
	token := r.Header.Get(AUTH_TOKEN)
	if token == "" {
		return "", ReturnError(errors.New("token not found"))
	}
	return token, nil
}

func (wrapper UserWrapper) Composite(ctx *model.AppContext, cd CompositeData) ([]byte, int, http.Header, error) {
	token, err := GetToken(cd.R)
	if err != nil {
		return nil, 400, nil, ReturnNewError("invalid-token")
	}
	exists, user := ValidateUser(token, ctx.Tokens)
	if !exists {
		return nil, 401, nil, ReturnNewError("invalid-token")
	}
	res, err := ctx.Db.Query("select role from users where id=?", user.Id)
	if err != nil {
		return nil, 500, nil, ReturnError(err)
	}
	role, _ := strconv.Atoi(string(res[0]["role"])) // ignore error cause database integrity
	if wrapper.UserLevel > role {
		return nil, 401, nil, ReturnNewError("role-insufficient")
	}
	user.UserLevel = role
	cd.User = user
	bytez, status, headers, err := wrapper.NextWrapper.Composite(ctx, cd)
	return bytez, status, headers, err
}

func (wrapper CacheWrapper) Composite(ctx *model.AppContext, cd CompositeData) ([]byte, int, http.Header, error) {
	bytez, status, headers, err := wrapper.NextWrapper.Composite(ctx, cd)
	if err != nil {
		return bytez, status, headers, ReturnError(err)
	}
	err = ctx.Mc.Set(&memcache.Item{Key: cd.R.RequestURI, Value: bytez, Expiration: /*in seconds*/ 60 * 10})
	if err != nil {
		return nil, 500, nil, ReturnError(err)
	}
	return bytez, status, headers, err
}

func (wrapper JSONDecoderWrapper) Composite(ctx *model.AppContext, cd CompositeData) ([]byte, int, http.Header, error) {
	body, err := ioutil.ReadAll(cd.R.Body)
	if err != nil {
		return nil, 500, nil, ReturnError(err)
	}
	var j model.JSON
	err = json.Unmarshal(body, &j)
	if err != nil {
		var l model.List
		err = json.Unmarshal(body, &l)
		if err != nil {
			return nil, 500, nil, ReturnError(err)
		}
		cd.JSONList = &l
	}
	cd.JSONMap = &j
	bytez, status, headers, err := wrapper.NextWrapper.Composite(ctx, cd)
	if err != nil {
		return bytez, status, headers, ReturnError(err)
	}
	return bytez, status, headers, err
}
