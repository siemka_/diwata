package webframework

import (
	"bytes"
	"diwata/model"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/smtp"
	"os"
	"os/exec"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"errors"

	"github.com/fatih/color"
	"github.com/sirupsen/logrus"
)

const AUTH_TOKEN = "X-Auth-Token"

func LoadConfiguration(file string) (*model.Config, error) {
	var config model.Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		return nil, ReturnError(err)
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		return nil, ReturnError(err)
	}
	return &config, nil
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	PrintfColored(color.BgMagenta, "ERROR 404 NOT FOUND")
	PrintfColored(color.FgMagenta, " "+r.RequestURI+"\n")
	http.Error(w, "{\"status\":\"error\"}", 404)
}

//SendMail trains battle snails
func SendMail(ctx *model.AppContext, to string, subject string, body string) error {
	// Connect to the remote SMTP server.
	c, err := smtp.Dial(ctx.Config.Mail.Server)
	if err != nil {
		return ReturnError(err)
	}
	defer c.Close()
	// Set the sender and recipient.
	err = c.Mail(ctx.Config.Mail.From)
	if err != nil {
		return ReturnError(err)
	}
	err = c.Rcpt(to)
	if err != nil {
		return ReturnError(err)
	}
	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		return ReturnError(err)
	}
	defer wc.Close()
	buf := bytes.NewBufferString(fmt.Sprintf("Subject: %s\r\n\r\n%s", subject, body))
	if _, err = buf.WriteTo(wc); err != nil {
		return ReturnError(err)
	}
	return nil
}

func SendGmail(to string, subject string, body string) error {
	from := "tt7164335@gmail.com"
	pass := "gabbaGabbaHey"

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject + "\n\n" +
		body

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))
	if err != nil {
		return ReturnError(err)
	}
	return nil
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUWXYZ0123456789"
const numberBytes = "1234567890"

func RandStringBytes(n int) string {
	return randString(n, letterBytes)
}

func RandStringNum(n int) string {
	return randString(n, numberBytes)
}

func randString(n int, bytez string) string {
	rand.Seed(time.Now().UTC().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = bytez[rand.Intn(len(bytez))]
	}
	return string(b)
}

type ErrorFormatter struct{}

func FieldToString(key string, value interface{}) string {
	switch key {
	case "err":
		return fmt.Sprintf("\x1b[31m%s\x1b[0m\n", value)
	case "line":
		return fmt.Sprintf("\x1b[33;1m:%d\x1b[0m\n", value)
	case "file":
		fullpath := value.(string)
		folders := strings.Split(fullpath, "/")
		parentpath := strings.Join(folders[:len(folders)-1], "/")
		return parentpath + fmt.Sprintf("\x1b[32;1m/%s\x1b[0m", folders[len(folders)-1])
	case "func":
		fullpath := value.(string)
		folders := strings.Split(fullpath, "/")
		parentpath := strings.Join(folders[:len(folders)-1], "/")
		return parentpath + fmt.Sprintf("\x1b[36;1m/%s\x1b[0m\n", folders[len(folders)-1])
	}
	return "LOGGER ERROR LOL"
}

func Deb(msg ...interface{}) {
	pc, _, line, _ := runtime.Caller(1)
	f := runtime.FuncForPC(pc)
	fmt.Println("!!! "+f.Name()+" ("+strconv.Itoa(line)+"): ", msg)
}

func (f *ErrorFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	if entry.Level == logrus.InfoLevel {
		if len(entry.Data) != 0 {
			fmt.Println("ENTRY HAS DATA")
		}
		return []byte(fmt.Sprintf("\x1b[0;94m%s\x1b[0m\n", ("INFO: " + entry.Message))), nil
	}

	if entry.Level == logrus.DebugLevel {
		return []byte("DEBUG: " + entry.Message + "\n"), nil
	}
	ret := make([]byte, 0)
	if entry.Data["err"] == nil {
		return ret, nil
	}
	entryErr := entry.Data["err"]
	if entryErr == nil {
		entryErr = ""
	}
	entryFile := entry.Data["file"]
	if entryFile == nil {
		entryFile = "<no file>"
	}
	entryLine := entry.Data["line"]
	if entryLine == nil {
		entryLine = -1
	}
	entryFunc := entry.Data["func"]
	if entryFunc == nil {
		entryFunc = "<no func>"
	}
	ret = append(ret, []byte(entry.Message+"\n")...)
	ret = append(ret, []byte(FieldToString("err", entryErr))...)
	ret = append(ret, []byte(FieldToString("file", entryFile)+FieldToString("line", entryLine))...)
	ret = append(ret, []byte(FieldToString("func", entryFunc))...)
	return ret, nil
}

//Fatal logs error and fails
func Fatal(errs ...error) {
	returnErrorEx(2, errs...) // ignore the return value
	fmt.Println("FATAL EXIT")
	os.Exit(1)
}

//Error logs errors in a unified way using logrus
func Error(errs ...error) {
	pc, file, line, _ := runtime.Caller(1)
	f := runtime.FuncForPC(pc)
	errStrs := make([]string, 0)
	for _, err := range errs {
		errStrs = append(errStrs, err.Error())
	}
	logrus.WithFields(logrus.Fields{
		"err":  strings.Join(errStrs, ", "),
		"line": line,
		"func": f.Name(),
		"file": file,
	}).Error()
}

//ReturnError logs error, but returns it, allowing returning of errors to be chained in a "stack trace"
func ReturnError(errs ...error) error {
	return returnErrorEx(2, errs...)
}

func returnErrorEx(caller int, errs ...error) error {
	newErrs := make([]error, 0)
	for _, v := range errs {
		if v != nil {
			newErrs = append(newErrs, v)
		}
	}
	errs = newErrs
	if len(errs) == 0 {
		return nil
	}
	pc, file, line, _ := runtime.Caller(caller)
	f := runtime.FuncForPC(pc)
	errStrs := make([]string, 0)
	for _, err := range errs {
		errStrs = append(errStrs, err.Error())
	}
	errStr := strings.Join(errStrs, ", ")
	logrus.WithFields(logrus.Fields{
		"err":  errStr,
		"line": line,
		"func": f.Name(),
		"file": file,
	}).Error("RETURNING")
	if len(errs) == 1 {
		return errs[0]
	}
	return errors.New(errStr)
}

func ReturnNewError(err string) error {
	return returnErrorEx(2, errors.New(err))
}

func GetJSONArg(cd CompositeData, argName string) string {
	if cd.JSONMap == nil {
		Deb("json map nil")
		return ""
	}
	val, ok := (*cd.JSONMap)[argName]
	if !ok {
		Deb(argName + " not found")
		return ""
	}
	str, ok := val.(string)
	if !ok {
		num, ok := val.(float64)
		if !ok {
			Deb(val, "not string, but ", reflect.ValueOf(val).String())
			return ""
		}
		return strconv.FormatFloat(num, 'f', -1, 64)
	}
	return str
}

//CdToBson doesn't change anything, it just makes the compiler happy because it is retarded
func CdToBson(d *model.JSON) *bson.M {
	var c map[string]interface{}
	var b bson.M
	var a *bson.M

	c = *d
	b = c
	a = &b
	return a
}

func NTHash(pass string) ([]byte, error) {
	out, err := exec.Command("smbencrypt", pass).Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(pass)
	tab := strings.Fields(string(out))
	return []byte(tab[1]), nil
}

func ResToJSON(res map[string][]byte) model.JSON {
	ret := make(model.JSON)
	for k, v := range res {
		ret[k] = string(v)
	}
	return ret
}

func ResToList(res []map[string][]byte) model.List {
	ret := make(model.List, 0)
	for _, m := range res {
		ret = append(ret, ResToJSON(m))
	}
	return ret
}
