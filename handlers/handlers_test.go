package handlers_test

import (
	"diwata/handlers"
	wf "diwata/webframework"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

type Ziom struct {
	GivenName  string
	GivenName2 string
	Surname    string
}

func detonate(err error) {
	if err != nil {
		wf.Error(err)
		os.Exit(1)
	}
}

func Test_test(t *testing.T) {
	zioms := make([]Ziom, 0)

	file, err := os.Open("names.txt") // For read access.
	detonate(err)
	dataBytes, err := ioutil.ReadAll(file)
	detonate(err)
	data := string(dataBytes)
	for _, row := range strings.Split(data, "\n") {
		rowFields := strings.Split(row, "\t")
		if rowFields[1] == "NULL" {
			rowFields[1] = ""
		}
		zioms = append(zioms, Ziom{
			rowFields[0], rowFields[1], rowFields[2],
		})
	}
	for _, z := range zioms {
		logins, err := handlers.GenerateAdLogins(z.GivenName, z.GivenName2, z.Surname)
		detonate(err)
		fmt.Println(strings.Join(logins, "|"))
	}
}

func Ttest_test(t *testing.T) {
	logins, err := handlers.GenerateAdLogins("Аlena", "", "Rantsevich")
	detonate(err)
	fmt.Println(strings.Join(logins, "|"))
}
