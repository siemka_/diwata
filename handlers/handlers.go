package handlers

import (
	"diwata/model"
	wf "diwata/webframework"
	"encoding/json"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"unicode"

	"github.com/davecgh/go-spew/spew"

	"github.com/google/uuid"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

const (
	USERS_TABLE     = "diwata_users"
	CHANGELOG_TABLE = "diwata_changelog"
	TOKENS_TABLE    = "diwata_tokens"
	AUTH_TOKEN      = "X-Auth-Token"
)

func _getFields(ctx *model.AppContext) (map[string]string, error) {
	metas, err := ctx.Db.DBMetas()
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	ret := make(map[string]string, 0)
	for _, table := range metas {
		if table.Name == USERS_TABLE {
			for _, c := range table.Columns() {
				ret[c.Name] = c.SQLType.Name
			}
		}
		if table.Name != CHANGELOG_TABLE &&
			table.Name != TOKENS_TABLE &&
			table.Name != USERS_TABLE &&
			strings.HasPrefix(table.Name, "diwata_") {
			// one of the variable tables
			// ret[table.Name[:len(table.Name)-1]] = "_table"
			ret[table.Name] = "_table"
		}
	}
	spew.Dump(ret)
	return ret, nil
}

var _fields map[string]string = nil

func getFields(ctx *model.AppContext) (map[string]string, error) {
	var err error
	if _fields == nil {
		_fields, err = _getFields(ctx)
		if err != nil {
			return nil, wf.ReturnError(err)
		}
	}
	return _fields, nil
}

func GetFieldsHandler(ctx *model.AppContext, cd wf.CompositeData) (interface{}, int) {
	fields, err := getFields(ctx)
	if err != nil {
		return wf.ReturnError(err), 500
	}
	retFields := make(model.JSON)
	for k, v := range fields {
		if v == "_table" {
			retFields[k[:len(k)-1]] = v
		} else {
			retFields[k] = v
		}
	}
	return model.JSON{"status": "ok", "data": retFields}, 200
}

func TestHandler(ctx *model.AppContext, cd wf.CompositeData) (interface{}, int) {
	return model.JSON{"status": "ok"}, 200
}

func checkUnique(ctx *model.AppContext, data model.JSON) (bool, error) {
	conditions := make([]string, 0)
	for k, v := range data {
		switch val := v.(type) {
		case model.List:
			for _, vv := range val {
				conditions = append(conditions, "json_contains("+k+", '\""+vv.(string)+"\"')")
			}
			break
		case string:
			conditions = append(conditions, k+" = '"+val+"'")
			break
		default:
			return false, wf.ReturnNewError("unsupported type " + reflect.ValueOf(v).Type().String())
		}
	}
	sql := "select * from " + USERS_TABLE + " where " + strings.Join(conditions, " or ")
	res, err := ctx.Db.Query(sql)
	if err != nil {
		return false, wf.ReturnError(err)
	}
	return len(res) == 0, nil
}

func getUserById(ctx *model.AppContext, id string) (model.JSON, error) {
	fields, err := getFields(ctx)
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	res, err := ctx.Db.Query("select * from "+USERS_TABLE+" where id=?", id)
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	if len(res) == 0 {
		return nil, wf.ReturnNewError("not-found")
	}
	user := wf.ResToJSON(res[0])
	for kk, v := range fields {
		k := kk[:len(kk)-1]
		if v == "_table" {
			sql := "select " + k + " from " + k + "s where owner=?"
			res, err := ctx.Db.Query(sql, id)
			if err != nil {
				return nil, wf.ReturnError(err)
			}
			l := wf.ResToList(res)
			user[k] = make(model.List, 0)
			for _, r := range l {
				user[k] = append(user[k].(model.List), r.(model.JSON)[k])
			}
		}
	}
	return user, nil
}

func getIDBySelector(ctx *model.AppContext, selector model.JSON) ([]string, error) {
	fields, err := getFields(ctx)
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	keys := make([]string, 0)
	values := make([]string, 0)
	ids := make([]string, 0)
	for kk, v := range selector {
		_, found := fields[kk]
		_, found2 := fields["diwata_"+kk+"s"]
		if !(found || found2) {
			// sql injection
			wf.Deb("INJECTION", kk)
		} else if fields[kk] == "VARCHAR" {
			// add condition
			keys = append(keys, kk)
			values = append(values, v.(string))
		} else if fields["diwata_"+kk+"s"] == "_table" {
			// do a select
			sql := "select owner from diwata_" + kk + "s where diwata_" + kk + "=?"
			res, err := ctx.Db.Query(sql, v.(string))
			if err != nil {
				return nil, wf.ReturnError(err)
			}
			if len(res) > 0 {
				owner := wf.ResToJSON(res[0])["owner"].(string)
				ids = append(ids, owner)
			}
		}
	}
	sql := "select id from " + USERS_TABLE + " where "
	for _, k := range keys {
		sql += k + "=? and "
	}
	if len(values) > 0 {
		valuesI := make([]interface{}, 0)
		valuesI = append(valuesI, sql[:len(sql)-4])
		for _, x := range values {
			valuesI = append(valuesI, x)
		}
		res, err := ctx.Db.Query(valuesI...)
		if err != nil {
			spew.Dump(valuesI)
			return nil, wf.ReturnError(err)
		}
		for _, r := range wf.ResToList(res) {
			ids = append(ids, r.(model.JSON)["id"].(string))
		}
	}
	return ids, nil
}

func getUsersByID(ctx *model.AppContext, ids []string) (model.List, error) {
	ret := make(model.List, 0)
	for _, id := range ids {
		u, err := getUserById(ctx, id)
		if err != nil {
			return nil, wf.ReturnError(err)
		}
		ret = append(ret, u)
	}
	return ret, nil
}

func isKeyValid(ctx *model.AppContext, key string) (bool, error) {
	cols, err := getFields(ctx)
	if err != nil {
		return false, wf.ReturnError(err)
	}
	for k := range cols {
		if (k == key || k == key+"s") && cols[k] == "VARCHAR" {
			return true, nil
		}
	}
	for k := range cols {
		if (k == "diwata_"+key || k == "diwata_"+key+"s") && cols[k] == "_table" {
			return true, nil
		}
	}
	return false, nil
}

func makeArray(val string, l int) []string {
	ret := make([]string, 0)
	for i := 0; i < l; i++ {
		ret = append(ret, val)
	}
	return ret
}

func cleanData(data *model.JSON) {
	keys := make([]string, 0)
	for k, v := range *data {
	switch_label:
		switch vv := v.(type) {
		case string:
			if strings.TrimSpace(vv) == "" {
				keys = append(keys, k)
			}
			break
		case model.List:
			for _, i := range vv {
				if strings.TrimSpace(i.(string)) != "" {
					break switch_label
				}
			}
			keys = append(keys, k)
			break
		default:
			break
		}
	}
	for _, k := range keys {
		delete(*data, k)
	}
}

func addRecord(ctx *model.AppContext, data model.JSON) (string, error) {
	id := uuid.New().String()
	keys := make([]string, 0)
	values := make([]interface{}, 0)
	sesh := ctx.Db.NewSession()
	err := sesh.Begin()
	if err != nil {
		return "", wf.ReturnError(err)
	}
	keys = append(keys, "id")
	values = append(values, id)

	cleanData(&data)

	for k, v := range data {
		valid, err := isKeyValid(ctx, k)
		if err != nil {
			return "", wf.ReturnError(err)
		}
		if !valid {
			return "", wf.ReturnNewError("invalid-key " + k)
		}
		switch val := v.(type) {
		case model.List:
			for _, vv := range val {
				_, err := sesh.Exec("insert into diwata_"+k+"s (owner,diwata_"+k+") values (?,?)", id, vv)
				if err != nil {
					err2 := sesh.Rollback()
					return "", wf.ReturnError(err, err2)
				}
			}
			break
		case string:
			keys = append(keys, k)
			values = append(values, val)
			break
		}
	}
	if data["ad_login"] == nil && data["given_name"] != nil && data["surname"] != nil {
		keys = append(keys, "ad_login")
		values = append(values, "")

		if data["given_name_2"] == nil {
			data["given_name_2"] = ""
		}

		ad_logins, err := GenerateAdLogins(data["given_name"].(string), data["given_name_2"].(string), data["surname"].(string), LOGIN_LENGTH_MAX)
		if err != nil {
			return "", wf.ReturnError(err)
		}
		i := 1
	outer_loop:
		for {
			for _, login := range ad_logins {
				finalLogin := login
				if i > 1 {
					finalLogin += strconv.Itoa(i)
				}
				values = values[:len(values)-1]
				values = append(values, finalLogin)
				var sql interface{} = "insert into " + USERS_TABLE + " (" + strings.Join(keys, ",") + ") values (" + strings.Join(makeArray("?", len(values)), ",") + ")"
				finalSql := append([]interface{}{sql}, values...)
				_, err = sesh.Exec(finalSql...)
				if err == nil {
					break outer_loop
				}
				if !strings.Contains(err.Error(), "Duplicate entry") || !strings.Contains(err.Error(), "idx_diwata_ad_login") {
					err2 := sesh.Rollback()
					return "", wf.ReturnError(err, err2)
				}
			}
			i++
		}
	} else {
		sql := "insert into " + USERS_TABLE + " (" + strings.Join(keys, ",") + ") values (" + strings.Join(makeArray("?", len(values)), ",") + ")"
		finalSql := append([]interface{}{sql}, values...)
		_, err = sesh.Exec(finalSql...)
		if err != nil {
			err2 := sesh.Rollback()
			return "", wf.ReturnError(err, err2)
		}
	}

	err = sesh.Commit()
	if err != nil {
		err2 := sesh.Rollback()
		return "", wf.ReturnError(err, err2)
	}
	return id, nil
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func intersect(target []string, deleted []string) []string {
	ret := make([]string, 0)
	for _, s := range target {
		if !contains(deleted, s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func listToStringlist(l model.List) []string {
	ret := make([]string, 0)
	for _, x := range l {
		ret = append(ret, x.(string))
	}
	return ret
}

func StringlistToList(l []string) model.List {
	ret := make([]interface{}, 0)
	for i := range l {
		ret = append(ret, &l[i])
	}
	return ret
}

func updateRecord(ctx *model.AppContext, id string, data model.JSON) error {
	keys := make([]string, 0)
	values := make([]interface{}, 0)
	sesh := ctx.Db.NewSession()
	err := sesh.Begin()
	if err != nil {
		return wf.ReturnError(err)
	}

	cleanData(&data)

	if !ctx.Config.SkipChangelog {
		oldData, err := getUserById(ctx, id)
		if err != nil {
			return wf.ReturnError(err)
		}
		oldDataBytes, err := json.Marshal(&oldData)
		if err != nil {
			return wf.ReturnError(err)
		}
		_, err = sesh.Exec("insert into "+CHANGELOG_TABLE+" (user_id, old_data, added) values (?,?,now())", id, string(oldDataBytes))
		if err != nil {
			return wf.ReturnError(err)
		}
	}

	for k, v := range data {
		valid, err := isKeyValid(ctx, k)
		if err != nil {
			return wf.ReturnError(err)
		}
		if !valid {
			return wf.ReturnNewError("invalid-key " + k)
		}
		switch val := v.(type) {
		case model.List:
			res, err := sesh.QueryString("select diwata_"+k+" from diwata_"+k+"s where owner=?", id)
			if err != nil {
				err2 := sesh.Rollback()
				return wf.ReturnError(err, err2)
			}
			alreadyPresent := make([]string, 0)
			for _, row := range res {
				alreadyPresent = append(alreadyPresent, row["diwata_"+k])
			}
			filteredVal := intersect(listToStringlist(val), alreadyPresent)
			if len(filteredVal) > 0 {
				sql := "insert into diwata_" + k + "s (owner,diwata_" + k + ") values " + strings.Join(makeArray("('"+id+"',?)", len(filteredVal)), ",")
				filteredValAny := StringlistToList(filteredVal)
				finalSql := append([]interface{}{sql}, filteredValAny...)
				_, err = sesh.Exec(finalSql...)
				if err != nil {
					err2 := sesh.Rollback()
					return wf.ReturnError(err, err2)
				}
			}
			break
		case string:
			keys = append(keys, k)
			values = append(values, val)
			break
		}
	}
	if len(keys) > 0 {
		vals := make([]string, 0)
		for _, k := range keys {
			vals = append(vals, k+"=?")
		}
		sql := "update " + USERS_TABLE + " set " + strings.Join(vals, ",") + " where id=?"
		values = append(values, id)
		finalSql := append([]interface{}{sql}, values...)
		_, err = sesh.Exec(finalSql...)
		if err != nil {
			err2 := sesh.Rollback()
			return wf.ReturnError(err, err2)
		}
	}

	err = sesh.Commit()
	if err != nil {
		err2 := sesh.Rollback()
		return wf.ReturnError(err, err2)
	}
	return nil
}

func verifyPostData(allowedFieldsRead []string, allowedFieldsWrite []string, postData model.JSON) bool {
	for k, _ := range postData["selector"].(model.JSON) {
		if !contains(allowedFieldsRead, k) {
			wf.Deb("wrong field", k)
			return false
		}
	}
	for k, _ := range postData["data"].(model.JSON) {
		if !contains(allowedFieldsWrite, k) {
			wf.Deb("wrong field", k)
			return false
		}
	}
	return true
}

func PostDataHandler(ctx *model.AppContext, cd wf.CompositeData) (interface{}, int) {
	token := cd.R.Header.Get(AUTH_TOKEN)
	allowedFieldsWrite, err := getPerms(ctx, "write", token)
	if err != nil {
		return wf.ReturnError(err), 500
	}
	allowedFieldsRead, err := getPerms(ctx, "read", token)
	if err != nil {
		return wf.ReturnError(err), 500
	}
	if !verifyPostData(allowedFieldsRead, allowedFieldsWrite, *cd.JSONMap) {
		return wf.ReturnNewError("forbidden-fields"), 401
	}
	if (*cd.JSONMap)["selector"] == nil || len((*cd.JSONMap)["selector"].(model.JSON)) == 0 {
		newUser, err := addRecord(ctx, (*cd.JSONMap)["data"].(model.JSON))
		if err != nil {
			if strings.Contains(err.Error(), "Duplicate entry") {
				return wf.ReturnNewError("dup-entry " + err.Error()), 400
			}
			return wf.ReturnError(err), 500
		}
		return model.JSON{"status": "ok", "new_user": newUser}, 200
	}
	userIDs, err := getIDBySelector(ctx, (*cd.JSONMap)["selector"].(model.JSON))
	if err != nil {
		return wf.ReturnError(err), 500
	}
	if len(userIDs) == 0 {
		return wf.ReturnNewError("not-found"), 404
	}
	updatedUserID := userIDs[0]
	err = updateRecord(ctx, updatedUserID, (*cd.JSONMap)["data"].(model.JSON))
	if err != nil {
		if strings.Contains(err.Error(), "Duplicate entry") {
			return wf.ReturnNewError("dup-entry " + err.Error()), 400
		}
		return wf.ReturnError(err), 500
	}
	return model.JSON{"status": "ok"}, 200
}

func getPerms(ctx *model.AppContext, perm, hash string) ([]string, error) {
	res, err := ctx.Db.QueryString("select field from diwata_tokens where hash=? and (permissions=? or permissions='readwrite')", hash, perm)
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	ret := make([]string, 0)
	for _, i := range res {
		ret = append(ret, i["field"])
	}
	return ret, nil
}

func dediwatize(str string) string {
	if len(str) < len("diwata_") {
		return str
	}
	return str[len("diwata_"):]
}

func filterUsersByAllowedFields(allowedFields []string, users model.List) model.List {
	ret := make(model.List, 0)
	for _, u := range users {
		newU := make(model.JSON)
		for k, v := range u.(model.JSON) {
			if contains(allowedFields, k) {
				newU[k] = v
			}
			if contains(allowedFields, dediwatize(k)) {
				newU[dediwatize(k)] = v
			}
		}
		ret = append(ret, newU)
	}
	return ret
}

func GetDataHandler(ctx *model.AppContext, cd wf.CompositeData) (interface{}, int) {
	selector := make(model.JSON)
	token := cd.R.Header.Get(AUTH_TOKEN)
	allowedFields, err := getPerms(ctx, "read", token)
	for k, v := range cd.R.URL.Query() {
		if !contains(allowedFields, k) {
			return wf.ReturnNewError("forbidden-field"), 401
		}
		selector[k] = v[0]
	}
	userIDs, err := getIDBySelector(ctx, selector)
	if err != nil {
		return wf.ReturnError(err), 500
	}
	users, err := getUsersByID(ctx, userIDs)
	if err != nil {
		return wf.ReturnError(err), 500
	}
	newUsers := filterUsersByAllowedFields(allowedFields, users)
	return model.JSON{"status": "ok", "data": newUsers}, 200
}

const (
	LOGIN_SEPARATOR  = "."
	LOGIN_LENGTH_MAX = 20
)

func splitSurname(surname string) []string {
	return strings.FieldsFunc(surname, func(r rune) bool {
		switch r {
		case ' ', '-':
			return true
		}
		return false
	})
}

func isSurnameMultipart(surname string) bool {
	return len(splitSurname(surname)) > 1
}

func truncateString(str string, l int) string {
	if len(str) > l {
		return str[:l]
	}
	return str
}

func normalizeSpaces(str string) string {
	space := regexp.MustCompile(`\s+`)
	ret := space.ReplaceAllString(str, " ")
	return ret
}

func removeDiacritics(str string) (string, error) {
	t := transform.Chain(norm.NFD, transform.RemoveFunc(func(r rune) bool {
		return unicode.Is(unicode.Mn, r)
	}), norm.NFC)
	result, _, err := transform.String(t, str)
	if err != nil {
		return "", wf.ReturnError(err)
	}
	r := strings.NewReplacer(
		"ı", "i",
		"ł", "l",
		// "đ", "d",
		// "ø", "o",
		// "ß", "ss",
		// "æ", "ae",
		// "¡", "i",
		// "°", "deg",
		// "а", "a",
	)
	return r.Replace(result), nil
}

func GenerateAdLogins(givenname, givenname2, surname string, login_length_max int) ([]string, error) {
	ret := make([]string, 0)
	// assert that givenname starts with a letter
	// delete - ´'` from surname
	r := strings.NewReplacer(
		"´", "",
		"'", "",
		"`", "",
	)

	givenname = strings.ToLower(r.Replace(givenname))
	givenname2 = strings.ToLower(r.Replace(givenname2))
	surname = strings.Replace(normalizeSpaces(strings.ToLower(r.Replace(surname))), " ", "-", -1)

	givenname, err := removeDiacritics(givenname)
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	givenname2, err = removeDiacritics(givenname2)
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	surname, err = removeDiacritics(surname)
	if err != nil {
		return nil, wf.ReturnError(err)
	}
	firstPart := givenname[:1]
	secondPart := ""
	if givenname2 != "" {
		secondPart = givenname2[:1]
	}
	thirdPart := surname
	firstLoginUncut := firstPart + LOGIN_SEPARATOR + thirdPart
	if len(firstLoginUncut) > login_length_max && isSurnameMultipart(thirdPart) {
		surnameParts := splitSurname(thirdPart)
		for i := len(surnameParts) - 1; i > 0; i-- {
			thirdPartShortened := strings.Join(surnameParts[:i], "-")
			firstLoginShortened := firstPart + LOGIN_SEPARATOR + thirdPartShortened
			if len(firstLoginShortened) <= login_length_max {
				ret = append(ret, firstLoginShortened)
				break
			}
		}
	}
	ret = append(ret, truncateString(firstLoginUncut, login_length_max))
	if secondPart != "" {
		ret = append(ret, truncateString(firstPart+secondPart+LOGIN_SEPARATOR+thirdPart, login_length_max))
	}
	return ret, nil
}

func GetChangesHandler(ctx *model.AppContext, cd wf.CompositeData) (interface{}, int) {
	id := cd.R.URL.Query().Get("id")
	res, err := ctx.Db.Query("select added, old_data from "+CHANGELOG_TABLE+" where user_id=?", id)
	if err != nil {
		return wf.ReturnError(err), 500
	}

	return model.JSON{"status": "ok", "changes": wf.ResToList(res)}, 200
}
