#!/usr/bin/python

import model, time

class PostcusMigration(model.Migration):

    name = "postcus"

    def migrate(self, stork, skips):
        stork.init_migration(10)
        for i in range(10):
            stork.incr()
        stork.finish_migration()
        print self.name + ", skips: " + str(skips)

postcus = PostcusMigration()