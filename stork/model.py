#!/usr/bin/python

import abc

class Migration:
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def migrate(self, stork, skips):
        pass