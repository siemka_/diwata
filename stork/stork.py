#!/usr/bin/python

import sys, abc

from model import Migration

from postcus import postcus
from adlogins import adlogins
from wybierzpoczte import wybierzpoczte
from zimbra import zimbra
from google import google
from dodajgoogle import dodajgoogle
from adssid import adssid

USAGE="""
usages:
    ./stork.py <migration_step>[+] [number]
    ./stork.py all
"""

MIGRATIONS=[
    postcus,
    adlogins,
    wybierzpoczte,
    zimbra,
    google,
    dodajgoogle,
    adssid
]

class Stork:
    current_migration = 0

    current_count = -1
    current_max_count = -1

    def init_migration(self, count):
        self.current_count = 0
        self.current_max_count = count
        print "STARTING: " + MIGRATIONS[self.current_migration].name
    
    def incr(self):
        if self.current_count < 0:
            self.init_migration(0)
        self.current_count += 1
        print ("\r"+str(self.current_count) +" / "+str(self.current_max_count)),
        sys.stdout.flush()
    
    def finish_migration(self):
        print "FINISHING: " + MIGRATIONS[self.current_migration].name
        self.current_count = -1
        self.current_max_count = -1

    def run(self):
        if len(sys.argv) == 1:
            print USAGE
            return 1

        migration_start_step = 0
        if len(sys.argv) >= 3:
            migration_start_step = int(sys.argv[2])

        continuing = False
        migrated = False
        if sys.argv[1][-1:] == "+":
            continuing = True
            migration_start = sys.argv[1][:-1]
        elif sys.argv[1] == "all":
            continuing = True
            migration_start = "postcus"
        else:
            migration_start = sys.argv[1]
        
        while self.current_migration < len(MIGRATIONS):
            if MIGRATIONS[self.current_migration].name != migration_start:
                self.current_migration += 1
                continue
            else:
                break
        
        if self.current_migration == len(MIGRATIONS):
            print "migration not found: " + str(migration_start)
            return 1

        MIGRATIONS[self.current_migration].migrate(self, migration_start_step)
        if self.current_count > 0:
            self.finish_migration()

        migration_start_step = 0
        self.current_migration += 1

        if continuing:
            while self.current_migration < len(MIGRATIONS):
                MIGRATIONS[self.current_migration].migrate(self, migration_start_step)
                if self.current_count > 0:
                    self.finish_migration()
                self.current_migration += 1
                migration_start_step = 0

        return 0

if __name__ == "__main__":
    s = Stork()
    sys.exit(s.run())