#!/bin/bash

mysql -uroot -proot diwata < ../ddl.sql

echo "RESTARTED DB"
rm config.json
ln -s ../config.json .

../pys/manage_perms.py add email readwrite hashhash
../pys/manage_perms.py add ad_login readwrite hashhash
../pys/manage_perms.py add google readwrite hashhash
../pys/manage_perms.py add facebook readwrite hashhash
../pys/manage_perms.py add sap_id readwrite hashhash
../pys/manage_perms.py add given_name readwrite hashhash
../pys/manage_perms.py add given_name_2 readwrite hashhash
../pys/manage_perms.py add surname readwrite hashhash
../pys/manage_perms.py add pesel readwrite hashhash
../pys/manage_perms.py add id readwrite hashhash

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "email": ["email0", "email1"],
        "google": ["google"],
        "facebook": ["facebook"],
        "sap_id": "sap_id",
        "given_name": "Marcin",
        "surname": "Parafiniuk"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "google": ["google2"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "email": ["email0"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "sap_id": "valid",
        "facebook": ["facebook"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "google": ["valid", "google"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi


data=$(curl -XGET http://localhost:8000/api/get?pesel=123 -H "X-Auth-Token: hashhash" 2> /dev/null|jq -r '.data')

if [ "$data" != "[]" ]; then
    echo $LINENO
    exit 1
fi

data=$(curl -XGET http://localhost:8000/api/get?sap_id=sap_id -H "X-Auth-Token: hashhash" 2> /dev/null|jq -r '.data[0].id')

if [ "$data" == "null" ]; then
    echo $LINENO
    exit 1
fi

data=$(curl -XGET http://localhost:8000/api/get?email=twojastarassiebrowara -H "X-Auth-Token: hashhash" 2> /dev/null|jq -r '.data')

if [ "$data" != "[]" ]; then
    echo $LINENO
    exit 1
fi

data=$(curl -XGET http://localhost:8000/api/get?email=email0 -H "X-Auth-Token: hashhash" 2> /dev/null|jq -r '.data[0].id')

if [ "$data" == "null" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
        "sap_id": "sap_id"
    },
    "data": {
        "pesel": "123",
        "google": ["g0", "g1", "google"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" == "error" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
        "sap_id": "sap_id"
    },
    "data": {
        "pesel": "123",
        "google": ["google", "google2"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi

data=$(curl -XGET http://localhost:8000/api/get?google=g0 -H "X-Auth-Token: hashhash" 2> /dev/null|jq -r '.data[0].id')

if [ "$data" == "null" ]; then
    echo $LINENO
    exit 1
fi

data=$(curl -XGET http://localhost:8000/api/get?google=google -H "X-Auth-Token: hashhash" 2> /dev/null|jq -r '.data[0].id')

if [ "$data" == "null" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Martyna",
        "surname": "Otrębska"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Martyna",
        "surname": "Otrębska"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Piotr",
        "given_name_2": "Alladyn",
        "surname": "Dulikowski",
        "sap_id": "69420"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Piotr",
        "given_name_2": "Alladyn",
        "surname": "Dulikowski",
        "sap_id": "69421"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Piotr",
        "given_name_2": "Alladyn",
        "surname": "Dulikowski",
        "sap_id": "69422"
    }
}
' 2> /dev/null|jq -r '.status')





status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Piotr",
        "given_name_2": "Alladyn",
        "surname": "Dulikowski",
        "sap_id": "69423"
    }
}
' 2> /dev/null|jq -r '.status')

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Piotr",
        "given_name_2": "Alladyn",
        "surname": "Dulikowski",
        "sap_id": "69424",
        "ad_login": "",
        "email": [""]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Piotr",
        "given_name_2": "Alladyn",
        "surname": "Dulikowski",
        "ad_login": "",
        "email": [""],
        "sap_id": "69425"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
    },
    "data": {
        "given_name": "Piotr",
        "given_name_2": "Alladyn",
        "surname": "Dulikowski",
        "ad_login": "",
        "email": [""],
        "sap_id": "69425"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" == "ok" ]; then
    echo $LINENO
    exit 1
fi


status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
        "sap_id": "69420"
    },
    "data": {
        "email": ["email1", "email2"],
        "ad_login": ""
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" == "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
        "sap_id": "69420"
    },
    "data": {
        "email": ["email3", "email4"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: hashhash" -d'
{
    "selector": {
        "sap_id": "69421"
    },
    "data": {
        "ad_login": ""
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XGET http://localhost:8000/api/get?google=google -H "X-Auth-Token: innyhash" 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi

../pys/manage_perms.py add google read innyhash

status=$(curl -XGET http://localhost:8000/api/get?google=google -H "X-Auth-Token: innyhash" 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

../pys/manage_perms.py add facebook write innyhash

status=$(curl -XGET http://localhost:8000/api/get?facebook=facebook -H "X-Auth-Token: innyhash" 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: innyhash" -d'
{
    "selector": {
        "facebook": "facebook"
    },
    "data": {
        "facebook": ["siemka"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi

../pys/manage_perms.py add facebook read innyhash

status=$(curl -XGET http://localhost:8000/api/get?facebook=facebook -H "X-Auth-Token: innyhash" 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -H "X-Auth-Token: innyhash" -d'
{
    "selector": {
        "facebook": "facebook"
    },
    "data": {
        "facebook": ["siemka"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi