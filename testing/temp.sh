#!/bin/bash

mysql -uroot -proot diwata < ../ddl.sql

echo "RESTARTED DB"

status=$(curl -XPOST http://localhost:8000/api/post -d'
{
    "selector": {
    },
    "data": {
        "email": ["email0", "email1"],
        "google": ["google"],
        "facebook": ["facebook"],
        "sap_id": "sap_id",
        "given_name": "Marcin",
        "surname": "Parafiniuk"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -d'
{
    "selector": {
    },
    "data": {
        "email": ["email0"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi