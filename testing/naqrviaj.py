#!/usr/bin/python

import requests, time
import numpy as np

with open("../handlers/names.txt") as f:
    content = f.read()

names = content.split("\n")
done = 0
times = []
for line in names:
    data = line.split("\t")

    if len(data) < 3:
        continue

    j = {
        "selector": {
        },
        "data": {
            "given_name": data[0],
            "given_name_2": ("" if data[1] == "NULL" else data[1]),
            "surname": data[2],
        }
    }

    start = time.time()
    r = requests.post("http://localhost:8000/api/post", json=j)
    end = time.time()

    if r.status_code >= 400:
        print r.status_code
        print r.text
        assert False
    done += 1
    elapsed = end - start
    times.append(elapsed)
    print str(done) + "/" + str(len(names)) + "\t" + str(elapsed) + "\t" + str(np.median(times)) + "\t" + ("{0:.2f}".format(float(done)/float(len(names))*100.0))+"%"

