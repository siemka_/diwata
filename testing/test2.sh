#!/bin/bash

mysql -uroot -proot diwata < ../ddl.sql

echo "RESTARTED DB"

status=$(curl -XPOST http://localhost:8000/api/post -d'
{
    "selector": {
    },
    "data": {
        "google": ["google2"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -d'
{
    "selector": {
    },
    "data": {
        "email": ["email0", "email1"],
        "google": ["google", "snoogle"],
        "facebook": ["facebook"],
        "sap_id": "sap_id",
        "given_name": "Marcin",
        "surname": "Parafiniuk"
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "ok" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -d'
{
    "selector": {
        "sap_id": "sap_id"
    },
    "data": {
        "pesel": "123",
        "google": ["g0", "g1", "google"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" == "error" ]; then
    echo $LINENO
    exit 1
fi

status=$(curl -XPOST http://localhost:8000/api/post -d'
{
    "selector": {
        "sap_id": "sap_id"
    },
    "data": {
        "pesel": "123",
        "google": ["google", "dupa", "google2"]
    }
}
' 2> /dev/null|jq -r '.status')

if [ "$status" != "error" ]; then
    echo $LINENO
    exit 1
fi