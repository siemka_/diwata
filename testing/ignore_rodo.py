#!/usr/bin/python

import mysql.connector, requests, time

db = mysql.connector.connect(
    host="10.30.87.21", user="dev", password="ASDkjvjhashdf9832jd92", database="cus")

c = db.cursor()

# os_id, pesel, firstname, secondname, lastname, index_nr, email

c.execute("select os_id, pesel, firstname, secondname, lastname, email from usos_users")
done = 0
errors = []
try:
    for i in c.fetchall():
        row = map(lambda x: str(x) if x is not None else "", i)

        j = {
            "selector": {
            },
            "data": {
                "os_id":        row[0],
                "pesel":        row[1],
                "given_name":   row[2],
                "given_name_2": row[3],
                "surname":     row[4],
                "email":        [row[5]]
            }
        }

        start = time.time()
        r = requests.post("http://localhost:8000/api/post", json=j)
        end = time.time()

        if r.status_code >= 400:
            errors.append((r.status_code, r.text))
        done += 1
        elapsed = end - start
        print(done, elapsed)
except Exception as e:
    print(e)
    print(errors)

print(errors)
