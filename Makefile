SRC:=$(shell find . -name "*.go")
TARGET_VM:=diwata@10.12.3.253

all: diwata

diwata: $(SRC)
	go build

deploy: diwata
	ssh $(TARGET_VM) 'killall diwata' || true
	scp diwata $(TARGET_VM):~
	scp ddl.sql $(TARGET_VM):~
	# ssh $(TARGET_VM) 'mysql -udiwata -pdiwata < ddl.sql'
	ssh $(TARGET_VM) './diwata > log 2>&1 &'