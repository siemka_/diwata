package model

import (
	"github.com/go-xorm/xorm"
	"github.com/olivere/elastic"

	"github.com/bradfitz/gomemcache/memcache"
)

type Config struct {
	Database struct {
		Address  string `json:"address"`
		Dbname   string `json:"dbname"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"database"`
	Host string `json:"host"`
	Port string `json:"port"`
	Mail struct {
		From   string `json:"from"`
		Server string `json:"server"`
	} `json:"mail"`
	ClientPass    string `json:"clientpass"`
	Profile       bool   `json:"profile"`
	SkipChangelog bool   `json:"skipchangelog"`
}

type AppContext struct {
	HostAddress string
	Mc          *memcache.Client
	Config      *Config
	Tokens      *map[string]*AuthorizedUser
	Db          *xorm.Engine
	Elastic     *elastic.Client
}

type JSON = map[string]interface{}
type List = []interface{}

type AuthorizedUser struct {
	Id        int
	UserLevel int
}
