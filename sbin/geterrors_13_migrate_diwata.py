#!/usr/bin/python
# job_uuid e25dfd82-c898-4b02-b286-ba56d42c569a

import requests, json, mysql.connector

j={
    "query": {
        "match_phrase" : {
            "job_uuid" : "e25dfd82-c898-4b02-b286-ba56d42c569a"
        }
    },
    "size": 10000
}

r = requests.post("http://localhost:9200/errors/_search", json=j)

data = json.loads(r.text)

hits = set()

for h in data['hits']['hits']:
    hits.add(int(h['_source']['row']))

db = mysql.connector.connect(
    host="localhost", user="diwata", password="diwata", database="diwata")

c = db.cursor()

c.execute("select os_id, ad_login, sap_id, exp_id from _users")

os_ids = []

count = 0
for i in c.fetchall():
    count += 1

    if count in hits:
        os_ids.append(int(i[0]))

people = []

for i in os_ids:
    r = requests.get("http://localhost:8000/api/get?os_id="+str(i))
    assert r.status_code == 200
    people.append(json.loads(r.text))

for p in people:
    print p['data'][0]['os_id'], p['data'][0]['ad_login'], p['data'][0]['role']