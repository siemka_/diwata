#!/usr/bin/python

import sys, requests

FIELD=sys.argv[1]
VALUE=sys.argv[2]

j={
    "query": {
        "match_phrase" : {
            FIELD : VALUE
        }
    },
    "size": 10000
}

r = requests.post("http://localhost:9200/errors/_search", json=j)
print(r.text)
