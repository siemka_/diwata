#!/usr/bin/python

import sys, requests

FIELD=sys.argv[1]
VALUE=sys.argv[2]

j={
    "query": {
        "match_phrase" : {
            FIELD : VALUE
        }
    }
}

r = requests.post("http://localhost:9200/errors/_delete_by_query", json=j)
print(r.status_code)
print(r.text)