import time, uuid, requests

job_uuid = uuid.uuid4()  # will be different every time script is launched
print("launched error engine: "+str(job_uuid))

def report_error(task, row, err):
    j = {
        'task': str(task),
        'error': str(err),
        'time': time.time(),
        'job_uuid': str(job_uuid),
        'row': int(row)
    }
    r = requests.post("http://localhost:9200/errors/_doc", json=j)
