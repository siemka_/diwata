#!/usr/bin/python

import requests, json

content = ""

with open("sapid.txt") as f:
    content = f.read()

count = 0
errors = []

expids = {}

nopesel = 0
doublenopesel = 0

for line in content.split("\n"):
    count += 1
    data = line.split(" ")
    if len(data) < 5:
        continue
    if data[2].strip() == "" or data[4].strip() == "":
        # print data[:]
        if data[6] == "," and data[8] == '':
            print " ".join(data)
        nopesel += 1
        continue
    
    
    pesel = data[2]
    expid = data[4].replace(",", "")

    expids[pesel] = expid

print "loaded file " + str(len(expids.keys()))
print "nopesel", nopesel

sys.exit(0)

for pesel in expids.keys():
    r = requests.get("http://localhost:8000/api/get?pesel="+pesel)
    j = json.loads(r.text)
    if len(j["data"]) == 0:
        print pesel

    """
    j = {
        "selector": {
            "pesel": pesel
        },
        "data": {
            "exp_id": expid
        }
    }

    r = requests.post("http://localhost:8000/api/post", json=j)

    if r.status_code != 200:
        print r.text
        errors.append(r.text)
    print count
    """

# print json.dumps(errors)