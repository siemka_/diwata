#!/usr/bin/python

import sys, json, mysql.connector, requests

USAGE="""
usage:
./manage_perms.py <action> [<field> <perm>] <hash>
actions: get, add, remove
fields: fields available in diwata as given in /api/getfields (the script does not check for field availability)
perms: read, write, readwrite


examples:
./manage_perms.py get jCb0l7FlVmKdvZQelYGUldKkB9OQHhss4hLubZxDYmRpU3tXJDvp
./manage_perms.py add email read jCb0l7FlVmKdvZQelYGUldKkB9OQHhss4hLubZxDYmRpU3tXJDvp
./manage_perms.py add sap_id readwrite jCb0l7FlVmKdvZQelYGUldKkB9OQHhss4hLubZxDYmRpU3tXJDvp
./manage_perms.py remove ad_login write jCb0l7FlVmKdvZQelYGUldKkB9OQHhss4hLubZxDYmRpU3tXJDvp
"""

if len(sys.argv) < 2:
    print USAGE
    sys.exit(0)

ACTION = sys.argv[1]

with open("config.json") as f:
    config = json.loads(f.read())

db = mysql.connector.connect(
    host=config['database']['address'], user=config['database']['username'], password=config['database']['password'], database=config['database']['dbname'])

c = db.cursor()

if ACTION == "get":
    print "perms for "+sys.argv[2]
    c.execute("select * from diwata_tokens where hash=%s", (sys.argv[2],))
    for i in c.fetchall():
        print i[2] +": "+ i[3]
if ACTION == "add":
    c.execute("insert into diwata_tokens (hash, field, permissions) values (%s,%s,%s)", (sys.argv[4], sys.argv[2], sys.argv[3]))
    db.commit()
    print "ok"
if ACTION == "remove":
    c.execute("delete from diwata_tokens where hash=%s and field=%s and permissions=%s", (sys.argv[4], sys.argv[2], sys.argv[3]))
    db.commit()
    print "rows affected", c.rowcount