#!/usr/bin/python

import sys, json, base64, requests, errors

LDAP_JSON_FILE="zimbra.json"

OSID_PREFIX="osid="

content = None

with open(LDAP_JSON_FILE) as f:
    content = json.loads(f.read())

count = 0

def report_weird(c):
    errors.report_error(__file__, count, "weird: "+str(c))
    print("weird: "+str(c))

for c in content:
    count += 1
    osid = 0
    try:
        notes = c['zimbraNotes'][0]
    except KeyError:
        report_weird(c)
        continue
    try:
        osid = int(notes)
    except ValueError:
        if notes.startswith(OSID_PREFIX):
            osid = int(notes[len(OSID_PREFIX):])
        else:
            """
            if notes[:2].lower() not in ["p:", "f:", "g:", "d:"]:
                if ": " in notes:
                    decoded_notes = base64.standard_b64decode(notes)
                    for line in decoded_notes.split("\n"):
                        if line.strip().startswith(OSID_PREFIX):
                            osid = int(notes[len(OSID_PREFIX):])
            """
            report_weird(c)
            continue
    if osid == 0:
        report_weird(c)
        continue
    j = {
        "selector": {
            "os_id": str(osid)
        },
        "data": {
            "email": c['mail']
        }
    }
    r = requests.post("http://localhost:8000/api/post", json=j)
    if r.status_code >= 400:
        errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
        print(r.status_code)
        print(r.text)
    print count

"""
with open("weirds.json", "w") as f:
    f.write(json.dumps(weirds))
print json.dumps(weirds)
"""

"""
print len(weirds)

with open("zimbraerrors.json", "w") as f:
    f.write(json.dumps(errors))
"""