#!/usr/bin/python

import mysql.connector, requests, time, json, errors

db = mysql.connector.connect(
    host="localhost", user="diwata", password="diwata", database="bazasql")

c = db.cursor()

c.execute("select os_id, target_address from mm_accounts")

count = 0

try:
    for i in c.fetchall():
        count += 1
        j = {
            "selector": {
                "os_id": str(i[0])
            },
            "data": {
                "email": [str(i[1])]
            }
        }

        print j

        r = requests.post("http://localhost:8000/api/post", json=j)

        if r.status_code >= 400:
            errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
            print(r.status_code)
            print(r.text)
        print count
except Exception as e:
    print("ERROR")
    print(e)

print "ok"