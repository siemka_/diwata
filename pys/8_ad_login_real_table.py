#!/usr/bin/python

import json, mysql.connector
from contextlib import closing

people = None

OSID='pleduPersonLId'
ADLOGIN='adLogin'

db = mysql.connector.connect(
    host="localhost", user="diwata", password="diwata", database="diwata")

with open("adloginy.json") as f:
    people = json.loads(f.read())

sql = "insert into adloginreal (osid,adlogin) values"
values = []

for p in people:
    if p.get(OSID, None) is None or p.get(ADLOGIN, None) is None:
        print "MISS", p
        continue
    values.append("("+p[OSID][0]+",'"+p[ADLOGIN][0]+"')")

sql = sql + ",".join(values)+";"

with closing( db.cursor() ) as c:
    c.execute(sql)

db.commit()
db.close()