#!/usr/bin/python

import mysql.connector, requests, time, json, errors, sys, traceback

SKIP = int(sys.argv[1])

db = mysql.connector.connect(
    host="localhost", user="diwata", password="diwata", database="diwata")

c = db.cursor()

c.execute("select os_id, ad_login, sap_id, exp_id from _users")

count = 0

try:
    for i in c.fetchall():
        count += 1
        if count < SKIP:
            continue

        try:
            a = str(i[1])
        except:
            errors.report_error(__file__, count, "not ascii")
            print("not ascii")
            continue

        j = {
            "selector": {
                "os_id": str(i[0])
            },
            "data": {
                "ad_login": str(i[1]),
                "sap_id": str(i[2]) if i[2] is not None else None,
                "exp_id": str(i[3]) if i[3] is not None else None,
            }
        }

        r = requests.post("http://localhost:8000/api/post", json=j)

        if r.status_code >= 400:
            errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
            print(r.status_code)
            print(r.text)
        print count
except:
    print("ERROR")
    print(traceback.format_exc())

print "ok"
print errors.job_uuid