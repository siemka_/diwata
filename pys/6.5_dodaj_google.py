#!/usr/bin/python

import requests, mysql.connector, json, errors, sys

db = mysql.connector.connect(
    host="localhost", user="diwata", password="diwata", database="pocztauwdodaj")

c = db.cursor()
c.execute("select account local_part, createdby adlogin1, managedby adlogin2 from created_accounts")

google_mails = []

count = 0

SKIPS=int(sys.argv[1])

def read_csv(content):
    for row in content.split("\n"):
        data = row.split(",")
        if len(data) >= 3:
            google_mails.append(data[2])

with open("emails0.csv") as f:
    content = f.read()
    read_csv(content)

with open("emails1.csv") as f:
    content = f.read()
    read_csv(content)

def diwata_get_by_adlogin(local_part):
    global count
    r = requests.get("http://localhost:8000/api/get?ad_login="+local_part)
    if r.status_code >= 400:
        errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
        print(r.status_code)
        print(r.text)
    j = json.loads(r.text)
    if len(j["data"]) == 0:
        return None
    return j["data"][0]["id"]

def check_google(mail):
    return mail in google_mails

def diwata_add(id, mail):
    global count
    j = {
        "selector": {
            "id": id
        },
        "data": {
            "email": [mail]
        }
    }
    r = requests.post("http://localhost:8000/api/post", json=j)
    e = json.loads(r.text)
    if r.status_code >= 400:
        errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
        print(r.status_code)
        print(r.text)

def diwata_get_by_mail(mail):
    global count
    r = requests.get("http://localhost:8000/api/get?email="+mail)
    if r.status_code >= 400:
        errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
        print(r.status_code)
        print(r.text)
    j = json.loads(r.text)
    if len(j["data"]) == 0:
        return None
    return j["data"][0]["id"]

if __name__ == "__main__":
    
    for row in c.fetchall():
        
        count += 1
        if count < SKIPS:
            continue
        print count

        local_part = row[0]
        adlogin1 = row[1]
        adlogin2 = row[2]

        id = diwata_get_by_adlogin(local_part)
        mail = local_part+"@uw.edu.pl"

        if id is not None:
            if check_google(mail):
                diwata_add(id, mail)
        elif adlogin2 is not None:
            id = diwata_get_by_adlogin(adlogin2)
            if id is None:
                id = diwata_get_by_mail(adlogin2)
            if id is None:
                errors.report_error(__file__, count, "adlogin2 "+adlogin2+" not found")
                continue
            if check_google(mail):
                diwata_add(id, mail)
        else:
            errors.report_error(__file__, count, "local_part not found "+local_part)
            continue

print "ok"
print errors.job_uuid