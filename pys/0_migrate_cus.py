#!/usr/bin/python

import mysql.connector, requests, time, json, errors

db = mysql.connector.connect(
    host="10.30.87.21", user="dev", password="ASDkjvjhashdf9832jd92", database="cus")

c = db.cursor()

# os_id, pesel, firstname, secondname, lastname, index_nr, email

c.execute("select usos_users.os_id, pesel, firstname, secondname, lastname, email, degree, usos_users.unit_code, www, tel1, tel2, nazwa_uzytkownika, t.c from usos_users left join (select os_id, count(role) c from usos_roles where role like 'PRAC%' group by os_id) t on usos_users.os_id = t.os_id")
# select usos_users.os_id, lastname, t.c from usos_users left join (select os_id, count(role) c from usos_roles where role like 'PRAC%' group by os_id) t on usos_users.os_id = t.os_id;
done = 0
try:
    for i in c.fetchall():
        row = map(lambda x: str(x) if x is not None else "", i)

        j = {
            "selector": {
            },
            "data": {
                "os_id":        row[0],
                "pesel":        row[1],
                "given_name":   row[2],
                "given_name_2": row[3],
                "surname":     row[4],
                "email":        [row[5]],
                "degree": row[6],
                "unit_code": row[7],
                "www": row[8],
                "tel1": row[9],
                "tel2": row[10],
                "nazwa_uzytkownika": row[11],
                "role": "p" if row[12] == "1" or row[12] == "2" else "n"
            }
        }

        """
| degree            | varchar(30)  | YES  |     | NULL    |       |
| unit_code         | varchar(20)  | YES  |     | NULL    |       |
| www               | varchar(100) | YES  |     | NULL    |       |
| tel1              | varchar(30)  | YES  |     | NULL    |       |
| tel2              | varchar(3
nazwa_uzytkownika
        """

        start = time.time()
        r = requests.post("http://localhost:8000/api/post", json=j)
        end = time.time()

        if r.status_code >= 400:
            errors.report_error(__file__, done, str(r.status_code) + " " + r.text)
            print(r.status_code)
            print(r.text)
        done += 1
        elapsed = end - start
        print(done, elapsed)
except Exception as e:
    print(e)
