#!/usr/bin/python

import sys, json

LDAP_FILE=sys.argv[1]

content = ""

with open(LDAP_FILE) as f:
    content = f.read()

people = []
current_person = {}

for line in content.split("\n"):
    if line.startswith("#"):
        continue

    if line.strip() == "":
        people.append(current_person)
        current_person = {}
        continue
    
    key = line.split(":")[0]
    if current_person.get(key, None) is None:
        current_person[key] = []
    current_person[key].append(":".join(line.split(":")[1:]).strip())

print json.dumps(people)