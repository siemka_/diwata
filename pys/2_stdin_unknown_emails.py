#!/usr/bin/python

import requests, mysql.connector, json, sys
from contextlib import closing

MISSES_FILE=sys.argv[1]

db = mysql.connector.connect(
    host="localhost", user="diwata", password="diwata", database="diwata")

misses = []

counter = 0
for i in sys.stdin:
    if '@' not in i:
        continue
    i = i[:-1]
    with closing( db.cursor() ) as c:
        c.execute("select * from emails where email=%s",(i,))
        if c.fetchone() is None:
            misses.append(i)
    counter += 1
    print counter


"""
    r = requests.get("http://localhost/api/get?email="+i[0])
    assert r.status_code == 200
    j = json.loads(r.text)
    if len(j['data']) == 0:
        misses.append(i[0])
    counter += 1
    print counter
"""

with open(MISSES_FILE, "w") as f:
    f.write(json.dumps(misses))
