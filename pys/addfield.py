#!/usr/bin/python

import sys

# -- #singlefields
# pesel varchar(255),

# -- #singleindexes
# create unique index idx_sap_id on _users(sap_id);

# -- #multitables
# create table emails (id serial, owner varchar(255), email varchar(255));

# -- #multiindexes
# create unique index idx_emails ON emails (email);

ddl = ""

def insertAfter(ddl, marker, text):
    lines = ddl.split("\n")
    c = 0
    for l in lines:
        if marker in l:
            break
        c += 1
    lines.insert(c+1, text)
    return "\n".join(lines)

FIELD_TYPE=sys.argv[1]
FIELD_NAME=sys.argv[2]

if __name__ == "__main__":

    with open("ddl.sql") as f:
        ddl = f.read()
    
    if FIELD_TYPE == "single":
        print "single"
        ddl = insertAfter(ddl, "#singlefields", FIELD_NAME + " varchar(255),")
        ddl = insertAfter(ddl, "#singleindexes", "create unique index idx_"+FIELD_NAME+" on _users("+FIELD_NAME+");")
    elif FIELD_TYPE == "multi":
        print "multi"
        ddl = insertAfter(ddl, "#multitables", "create table "+FIELD_NAME+"s (id serial, owner varchar(255), "+FIELD_NAME+" varchar(255));")
        ddl = insertAfter(ddl, "#multiindexes", "create unique index idx_"+FIELD_NAME+"s ON "+FIELD_NAME+"s ("+FIELD_NAME+");")
    else:
        print "unknown field type"

    with open("ddl.sql", "w") as f:
        f.write(ddl)