#!/usr/bin/python

import requests, json

content = ""

with open("sapid.txt") as f:
    content = f.read()

count = 0
errors = []

expids = {}

for line in content.split("\n"):
    count += 1
    data = line.split(" ")
    if len(data) < 5:
        continue
    
    expid = data[4].replace(",", "")

    expids[expid] = data

print "loaded file " + str(len(expids.keys()))

for expid in expids.keys():
    if expids[expid][2] == "":
        data = expids[expid][6:]
        i = 0
        imie = []
        nazwisko = []
        while data[i] != "nazwisko:":
            imie.append(data[i])
            i += 1
        nazwisko = data[i+1:]
        imie[-1] = imie[-1][:-1]
        imie = " ".join(imie) 
        nazwisko = " ".join(nazwisko)
        if imie.strip() == "" or nazwisko.strip() == "":
            print "NO NAME", expid
            continue
        # print imie, nazwisko
        r = requests.get("http://localhost:8000/api/get?given_name="+imie+"&surname="+nazwisko)
        j = json.loads(r.text)
        if len(j["data"]) == 1:
            # jackpot
            p = {
                "selector": {
                    "id": j["data"][0]["id"]
                },
                "data": {
                    "exp_id": expid
                }
            }
            # print p
            r = requests.post("http://localhost:8000/api/post", json=p)
            assert r.status_code == 200
        else:
            print "NAME MISMATCH", imie, nazwisko, len(j["data"])

    """
    r = requests.get("http://localhost:8000/api/get?pesel="+pesel)
    j = json.loads(r.text)
    if len(j["data"]) == 0:
        print pesel

    j = {
        "selector": {
            "pesel": pesel
        },
        "data": {
            "exp_id": expid
        }
    }

    r = requests.post("http://localhost:8000/api/post", json=j)

    if r.status_code != 200:
        print r.text
        errors.append(r.text)
    print count
    """

# print json.dumps(errors)