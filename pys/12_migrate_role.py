#!/usr/bin/python

import mysql.connector, requests, errors

db = mysql.connector.connect(
    host="10.30.87.21", user="dev", password="ASDkjvjhashdf9832jd92", database="cus")

c = db.cursor()

c.execute("select os_id, role from usos_roles order by os_id")

count = 0

def send_request(os_id, cur_roles, count):
    j = {
        "selector": {
            "os_id": os_id
        },
        "data": {
            "role": cur_roles
        }
    }

    r = requests.post("http://localhost:8000/api/post", json=j)

    if r.status_code >= 400:
        errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
        print(r.status_code)
        print(r.text)

current_osid = 0

roles = {}

try:
    for i in c.fetchall():
        count += 1
        
        if str(i[0]) != current_osid:
            # new osid, send request
            cur_roles = ""
            if roles.get('PRAC_ETAT', None):
                cur_roles += "e"
            if roles.get('PRAC_NIEETAT', None):
                cur_roles += "n"
            if cur_roles != "":
                send_request(current_osid, cur_roles, count)
            roles = {}
            current_osid = str(i[0])
        
        roles[str(i[1])] = True
        
        print count
    
    cur_roles = ""
    if roles.get('PRAC_ETAT', None):
        cur_roles += "e"
    if roles.get('PRAC_NIEETAT', None):
        cur_roles += "n"
    send_request(current_osid, cur_roles, count)

except:
    print("ERROR")
    print(traceback.format_exc())

print "ok"