#!/usr/bin/python

import mysql.connector, requests, errors, json

def dict_list_add(d, k, v):
    l = d.get(k, [])
    l.append(v)
    d[k] = l

db = mysql.connector.connect(
    host="10.30.87.21", user="dev", password="ASDkjvjhashdf9832jd92", database="cus")

c = db.cursor()

c.execute("select * from usos_functions")

os_ids = set()

functions = {}

for i in c.fetchall():
    data = {
        "unit_code": str(i[1]),
        "fun_code": str(i[3]),
        "begin_date": str(i[4]),
        "end_date": str(i[5]),
        "description": str(i[6]),
        "fun_name": str(i[7]),
        "fun_name_en": str(i[8])
    }
    dict_list_add(functions, str(i[2]), json.dumps(data))
    os_ids.add(str(i[2]))

occupations = {}
c.close()
c = db.cursor()
c.execute("select * from usos_occupations")

for i in c.fetchall():
    data = {
        "unit_code": str(i[1]),
        "begin_date": str(i[3]),
        "end_date": str(i[4]),
        "occ_name": str(i[5]),
        "occ_name_en": str(i[6]),
    }
    dict_list_add(occupations, str(i[2]), json.dumps(data))
    os_ids.add(str(i[2]))

count = 0
for os_id in os_ids:
    count += 1
    j = {
        "selector": {
            "os_id": os_id
        },
        "data": {
            "function": functions.get(os_id, []),
            "occupation": occupations.get(os_id, [])
        }
    }

    r = requests.post("http://localhost:8000/api/post", json=j)

    if r.status_code >= 400:
        errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
        print(r.status_code)
        print(r.text)
    print count , "/", len(os_ids)