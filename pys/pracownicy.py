#!/usr/bin/python

import requests, json

with open("pracownicy.csv") as f:
    content = f.read()

for row in content.split("\n"):
    l = row.split(",")
    if len(l) == 1:
        print l
        continue
    r = requests.get("http://localhost:8001/api/get?email="+l[2])
    print json.loads(r.text)['data'][0]['email'][0] +","+ json.loads(r.text)['data'][0]['sap_id']