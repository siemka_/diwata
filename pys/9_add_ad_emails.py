#!/usr/bin/python

import requests, json, os, sys, errors

SKIPPED=int(sys.argv[1])

content = None

with open("adloginy.json") as f:
    content = json.loads(f.read())

counter = 0
try:
    for i in content:
        if counter < SKIPPED or i.get('pleduPersonLId', None) is None or i.get('mail', None) is None:
            counter += 1
            continue
        j = {
            "selector": {
                "os_id": str(i['pleduPersonLId'][0])
            },
            "data": {
                "email": [str(i['mail'][0])]
            }
        }

        # print j

        r = requests.post("http://localhost:8000/api/post", json=j)

        if r.status_code >= 400:
            errors.report_error(__file__, counter, str(r.status_code) + " " + r.text)
            print(r.status_code)
            print(r.text)
        counter += 1
        print counter
except Exception as e:
    print("ERROR")
    print(e)

print errors.job_uuid
print "ok"
