#!/usr/bin/python

import mysql.connector, requests, time, json, sys, errors
from contextlib import closing

LIMIT_100 = " limit 100"
SKIP = int(sys.argv[1])

db = mysql.connector.connect(
    host="localhost", user="diwata", password="diwata", database="bazasql")

count = 0
try:
    with closing( db.cursor() ) as c:
        c.execute("select os_id, local_part from user_local_part left join users on user_local_part.user_id=users.user_id")
        for i in c.fetchall():
            if count < SKIP:
                count += 1
                continue
            if i[0] is None:
                count += 1
                continue
            j = {
                "selector": {
                    "os_id": str(i[0])
                },
                "data": {
                    "email": [i[1]+"@uw.edu.pl"]
                }
            }
            r = requests.post("http://localhost:8000/api/post", json=j)

            if r.status_code >= 400:
                errors.report_error(__file__, count, str(r.status_code) + " " + r.text)
                print(r.status_code)
                print(r.text)
            count += 1
            print count
except Exception as e:
    print("ERROR")
    print(e)

print "ok"