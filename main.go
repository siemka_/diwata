package main

import (
	"diwata/handlers"
	"diwata/model"
	wf "diwata/webframework"
	"fmt"
	"net/http"
	"time"

	"github.com/davecgh/go-spew/spew"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"

	"github.com/pkg/profile"
)

/*
TODOs:
first launch get post bug
utime vs insert_utime
*/

const CONFIG_PATH = "./config.json"

const (
	Driver     = "mysql"
	Datasource = "%s:%s@tcp(%s)/%s"
	CpuProfile = "cpu.prof"
)

func main() {
	var err error

	logrus.SetFormatter(&wf.ErrorFormatter{})
	logrus.SetLevel(logrus.DebugLevel)

	config, err := wf.LoadConfiguration(CONFIG_PATH)
	if err != nil {
		wf.Fatal(err)
	}
	tokens := make(map[string]*model.AuthorizedUser)
	dbConnectString := fmt.Sprintf(Datasource, config.Database.Username, config.Database.Password, config.Database.Address, config.Database.Dbname)
	db, err := xorm.NewEngine(Driver, dbConnectString)
	/*db.ShowSQL(true)
	db.SetLogLevel(xorm.DEFAULT_LOG_LEVEL)*/
	if err != nil {
		wf.Fatal(err)
	}

	if config.Profile {
		defer profile.Start(profile.CPUProfile).Stop()
	}

	/*
		client, err := elastic.NewClient()
		if err != nil {
			panic(err)
		}*/

	ctx := model.AppContext{Config: config, Tokens: &tokens, Db: db}

	r := mux.NewRouter()
	r.NotFoundHandler = http.HandlerFunc(wf.NotFound)

	sub := r.PathPrefix("/api/").Subrouter()
	sub.Handle("/test", finalHandler(&ctx, handlers.TestHandler))
	sub.Handle("/post", finalPostHandler(&ctx, handlers.PostDataHandler))
	sub.Handle("/get", finalHandler(&ctx, handlers.GetDataHandler))
	sub.Handle("/getchanges", finalHandler(&ctx, handlers.GetChangesHandler))

	sub.Handle("/fields", finalHandler(&ctx, handlers.GetFieldsHandler))

	handler := cors.Default().Handler(r)

	s := http.Server{
		Addr:           ctx.Config.Host + ":" + ctx.Config.Port,
		Handler:        handler,
		ReadTimeout:    30 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	logrus.Info("starting diwata BACKEND")
	spew.Dump(ctx.Config)
	err = s.ListenAndServe()
	if err != nil {
		wf.Error(err)
	}
}

func finalHandler(ctx *model.AppContext, handle func(*model.AppContext, wf.CompositeData) (interface{}, int)) wf.FinalWrapper {
	return wf.FinalWrapper{Ctx: ctx,
		NextWrapper: wf.JSONOperator{Handle: handle}}
}

func finalUserHandler(ctx *model.AppContext, userLevel int, handle func(*model.AppContext, wf.CompositeData) (interface{}, int)) wf.FinalWrapper {
	return wf.FinalWrapper{
		Ctx: ctx,
		NextWrapper: wf.UserWrapper{
			UserLevel: userLevel,
			NextWrapper: wf.JSONOperator{
				Handle: handle,
			},
		},
	}
}

func finalPostHandler(ctx *model.AppContext, handle func(*model.AppContext, wf.CompositeData) (interface{}, int)) wf.FinalWrapper {
	return wf.FinalWrapper{
		Ctx: ctx,
		NextWrapper: wf.JSONDecoderWrapper{
			NextWrapper: wf.JSONOperator{
				Handle: handle,
			},
		},
	}
}

func finalPostUserHandler(ctx *model.AppContext, userLevel int, handle func(*model.AppContext, wf.CompositeData) (interface{}, int)) wf.FinalWrapper {
	return wf.FinalWrapper{
		Ctx: ctx,
		NextWrapper: wf.UserWrapper{
			UserLevel: userLevel,
			NextWrapper: wf.JSONDecoderWrapper{
				NextWrapper: wf.JSONOperator{
					Handle: handle,
				},
			},
		},
	}
}
