drop table if exists diwata_users;
drop table if exists diwata_changelog;
drop table if exists diwata_facebooks;
drop table if exists diwata_googles;
drop table if exists diwata_emails;
drop table if exists diwata_tokens;
drop table if exists diwata_occupations;
drop table if exists diwata_functions;

create table diwata_users (
    id varchar(255),
    -- #singlefields
role varchar(255),
nazwa_uzytkownika varchar(255),
tel2 varchar(255),
tel1 varchar(255),
www varchar(255),
unit_code varchar(255),
degree varchar(255),
sap_id varchar(255),
exp_id varchar(255),
os_id varchar(255),
ad_login varchar(255),
surname varchar(255),
given_name_2 varchar(255),
given_name varchar(255),
pesel varchar(255),

    primary key(id)
);

create table diwata_tokens (
    id serial,
    hash varchar(255),
    field varchar(255),
    permissions enum('read', 'write', 'readwrite')
);

create unique index idx_diwata_tokens on diwata_tokens(hash, field, permissions);

-- #singleindexes
create index idx_diwata_role on diwata_users(role);
create unique index idx_diwata_nazwa_uzytkownika on diwata_users(nazwa_uzytkownika);
create unique index idx_diwata_sap_id on diwata_users(sap_id);
create unique index idx_diwata_os_id on diwata_users(os_id);
create unique index idx_diwata_ad_login on diwata_users(ad_login);
create unique index idx_diwata_pesel on diwata_users(pesel);
create unique index idx_diwata_exp_id on diwata_users(exp_id);

-- #multitables
create table diwata_occupations (id serial, owner varchar(255), index occupation_owner (owner), diwata_occupation varchar(255));
create table diwata_functions (id serial, owner varchar(255), index function_owner (owner), diwata_function varchar(255));
create table diwata_facebooks (id serial, owner varchar(255), index facebook_owner (owner), diwata_facebook varchar(255));
create table diwata_googles (id serial, owner varchar(255), index google_owner (owner), diwata_google varchar(255));
create table diwata_emails (id serial, owner varchar(255), index email_owner (owner), diwata_email varchar(255));

-- #multiindexes
create unique index idx_diwata_functions ON diwata_functions (diwata_function);
create unique index idx_diwata_occupations ON diwata_occupations (diwata_occupation);
create unique index idx_diwata_facebooks ON diwata_facebooks (diwata_facebook);
create unique index idx_diwata_googles ON diwata_googles (diwata_google);
create unique index idx_diwata_emails ON diwata_emails (diwata_email);

create table diwata_changelog (
    id serial,
    user_id varchar(255),
--    old_data json,
    added timestamp
);
