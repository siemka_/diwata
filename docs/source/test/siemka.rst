.. Diwata documentation master file, created by
   sphinx-quickstart on Wed Apr  3 14:02:45 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

elo
==================================


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

testowy tekst bez linii

This is a paragraph.  It's quite
short.

    This paragraph will result in an indented block of
    text, typically used for quoting other text.

This is another one.

normal *italics* **bold**

`one backtick`

``two backticks``

``*``

1. numbers

A. upper-case letters
   and it goes over many lines

   with two paragraphs and all!

a. lower-case letters

   3. with a sub-list starting at a different number
   4. make sure the numbers are in the correct sequence though!

I. upper-case roman numerals

i. lower-case roman numerals

(1) numbers again

1) and again

* a bullet point using "*"

  - a sub-list using "-"

    + yet another sub-list

  - another item

what
  Definition lists associate a term with a definition.

*how*
  The term is a one-line phrase, and the definition is one or more
  paragraphs or body elements, indented relative to the term.
  Blank lines are not allowed between term and definition.

An example::

    Whitespace, newlines, blank lines, and all kinds of markup
      (like *this* or \this) is preserved by literal blocks.
  Lookie here, I've dropped an indentation level
  (but not far enough)

no more example

::

    This is preformatted text, and the
    last "::" paragraph is removed

a na tej stronie_ jest python?

.. _stronie: http://www.python.org/

Clicking on this internal hyperlink will take us to the target_
below.

.. _target:

The hyperlink target above points to this paragraph.

Chapter 1 Title
===============

Section 1.1 Title
-----------------

Subsection 1.1.1 Title
~~~~~~~~~~~~~~~~~~~~~~

Subsubsection 1.1.1.1 siemka
+++++++++++++++++++++++++++++++++

Section 1.2 Title
-----------------

Chapter 2 Title
===============



siemka witam serdecznie chciałbym napisać jakiś drugi czapter

a na tej stronie_ jest python?

.. _stronie: http://www.python.org/

Clicking on this internal hyperlink will take us to the target2_
below.

.. _target2:

The hyperlink target above points to this paragraph.