DIWATA
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Diwata to centralny rejestr osób na Uniwersytecie Warszawskim.

Każda osoba ma przypisany jeden rekord. W tym rekordzie są pola **pojedyncze** i **wielokrotne**.

pole pojedyncze (**single**)
  Pole, które każda osoba ma dokładnie jedno, np. PESEL albo os_id.

pole wielokrotne (**multi**)
  Pole, którego każda osoba może mieć kilka, np. mail, kontakt facebookowy albo funkcja na UW.

Żeby wykonać request do Diwaty należy mieć aktualny token, przekazywany w nagłówku HTTP. Do tego tokena przypisane są uprawnienia do odczytu i zapisu określonych pól.

endpointy
==========

*[GET]* /api/fields
------------------------

Endpoint do pytania o aktualne pola znajdujące się w Diwacie.

``curl http://diwata/api/fields``

.. code-block:: json

   {
     "data": {
       "ad_login": "VARCHAR",
       "email": "_table",
       ...
     },
     "status": "ok"
   }

Dane są w formacie <nazwa pola>:<typ>. Typ może być albo ``VARCHAR`` albo ``_table``. Oznacza to odpowiednio pole single i multi.

*[POST]* /api/post
-------------------

Endpoint do wysyłania i modyfikowania danych w Diwacie.

``curl -XPOST http://diwata/api/post -d@post.json -H "X-Auth-Token: secrettoken"``

.. code-block:: json

   {
     "selector": {
       "ad_login": "mc.parafiniuk",
       "email": "mc.parafiniuk@uw.edu.pl"
     },
     "data": {
       "sap_id": "50000123123",
       "email": ["siemka@tasiemka.pl"]
     }
   }

Body requesta jest jsonem jak w przykładzie. Obiekt "selector" zawiera dane, po których chcemy znaleźć rekord. Wyszukiwanie jest po wszystkich polach naraz (**koniunkcja, AND**). Selector może być **pusty**.

"Data" zawiera dane, które chcemy dodać. Jeśli pole jest typu single i ma już jakąś wartość, zostanie zmodyfikowane. Jeśli pole jest typu multi, Diwata spróbuje dodać nową wartość. Niektóre pola multi mają założone unikalne indeksy - wtedy ta operacja może się nie udać (ta wartość należy już do kogoś innego) i dostaniemy odpowiedni błąd.

**SUPER WAŻNE: w "data" pola multi muszą być podane w liście a nie jako zwykła wartość.** Jeśli to przegapimy poleci 500. W selektorze pola są podawane bez list.

Endpoint zwraca jsona:

.. code-block:: json

   {
     "status": "ok"
   }

w przypadku udanej operacji albo jsona z błędem.

*[GET]* /api/get
-------------------

``curl -XGET http://diwata/api/get?given_name=Marcin&surname=Parafiniuk -H "X-Auth-Token: secrettoken"``

Endpoint do zapytań do Diwaty.

Link jest w postaci <nazwa pola>=<wartość>&<inna nazwa>=<inna wartość>, tak jak w normalnym requeście GET.

Dostajemy dane w postaci:

.. code-block:: json

  {
    "data": [
      {
        "ad_login": "mc.parafiniuk",
        "email": [
          "mc.parafiniuk@student.uw.edu.pl",
          "mc.parafiniuk@g.student.uw.edu.pl"
        ],
        ...
      },
      ...
    ],
    "status": "ok"
  }

Jeśli ``status`` jest ``ok``, to ``data`` będzie zawierać **listę** osób pasujących do podanych kryteriów. Kryteria są istotne wszystkie naraz (jak poprzednio, **AND**). Każdy obiekt w liście będzie zawierał wszystkie dane, do których token requesta ma dostęp. Pola single są podane jako wartość, a pola multi jako lista wartości.